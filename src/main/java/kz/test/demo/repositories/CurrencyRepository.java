package kz.test.demo.repositories;

import kz.test.demo.models.CurrencyRate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface CurrencyRepository extends JpaRepository<CurrencyRate, Integer> {
    CurrencyRate findFirstBySrcCurrencyAndLastUpdate(String currency, LocalDateTime dateTime);
}
