package kz.test.demo.services;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface CurrencyService {
    BigDecimal getCurrencyRate(String currency, LocalDateTime dateTime);
}
