package kz.test.demo.services;

import kz.test.demo.repositories.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Service
public class CurrencyServiceImpl implements CurrencyService {
    @Autowired
    private CurrencyRepository repository;

    @Override
    public BigDecimal getCurrencyRate(String currency, LocalDateTime dateTime) {
        return repository.findFirstBySrcCurrencyAndLastUpdate(currency, dateTime).getRate();
    }
}
