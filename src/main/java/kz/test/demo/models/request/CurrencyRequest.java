package kz.test.demo.models.request;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CurrencyRequest {
    private String currencyCode;
    private LocalDateTime exchangeDate;
}
