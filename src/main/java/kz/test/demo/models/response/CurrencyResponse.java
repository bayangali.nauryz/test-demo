package kz.test.demo.models.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class CurrencyResponse {
    BigDecimal value;
}
