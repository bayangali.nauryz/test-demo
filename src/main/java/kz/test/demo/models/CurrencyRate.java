package kz.test.demo.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity(name = "currency_rates")
public class CurrencyRate {
    @Id
    @NotNull
    private int id;
    @NotNull
    @Column(name = "srccurrency")
    private String srcCurrency;
    @NotNull
    @Column(name = "dstcurrency")
    private String dstCurrency;
    @Column(name = "lastupdate")
    private LocalDateTime lastUpdate;
    @NotNull
    private BigDecimal rate;
}
