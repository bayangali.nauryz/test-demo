package kz.test.demo.controllers;

import kz.test.demo.models.request.CurrencyRequest;
import kz.test.demo.models.response.CurrencyResponse;
import kz.test.demo.services.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@RestController
public class CurrencyController {
    //TODO irresistable API need to be designed )))

    @Autowired
    private CurrencyService service;

    @PostMapping
    public CurrencyResponse getCurrencyDate(@RequestBody CurrencyRequest request){
        BigDecimal result = service.getCurrencyRate(request.getCurrencyCode(), request.getExchangeDate());
        return response(result);
    }

    @GetMapping("/{currencyCode}/{exchangeDate}")
    public CurrencyResponse getCurrencyDate(@PathVariable("currencyCode") String currencyCode,
                                      @PathVariable("exchangeDate")
                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime exchangeDate) {
        BigDecimal result = service.getCurrencyRate(currencyCode, exchangeDate);
        return response(result);
    }

    protected CurrencyResponse response(BigDecimal result){
        return new CurrencyResponse(result);
    }
}
