drop table if exists currency_rates;

create table currency_rates ( 
    id INT not null, 
    srcCurrency VARCHAR(3) not null, 
    dstCurrency VARCHAR(3) not null, 
    lastUpdate timestamp default CURRENT_TIMESTAMP, 
    rate numeric(18, 4) not null 
); 